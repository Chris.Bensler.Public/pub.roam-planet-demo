#pragma once
#include <assert.h>
#include "SFML/Graphics.hpp"
#include "Math/Physics.h"
#include "Math/Vector.h"
#include "Math/Noise.h"
#include "LinkedList.h"
#include "Types.h"

#define APP_TITLE "PlanetGen"

#define SCREEN_W 1024
#define SCREEN_H 768

#define FOVY  30.0
#define ZNEAR 0.1
#define ZFAR  2500.0

#define ZFAR_MAX  (2000) // distance to desired far plane
#define ZFAR_ERR  1000000.0 // everything beyond ZFAR_ERR is rendered at ZFAR_MAX
#define ZFAR_HALF (ZFAR_MAX * 0.5) // everything between ZFAR_HALF and ZFAR_ERR is scaled exponentially between ZFAR_HALF and ZFAR_MAX

#define MAX_LOD 8

class C3DObject {
public:
    CQuaternion Rot;
    CVector3d Pos;
    CVector3 Velocity;
    C3DObject *Parent;
    float32 Scale;

    C3DObject() : Rot(), Pos(), Velocity() { Parent = NULL; Scale = 1.0f; }

    void UpdatePos(float32 seconds) { Pos += Velocity * seconds; }
    void UpdatePos(int32 milli) { UpdatePos(milli * 0.001f); }

    CMatrix GetViewMatrix() {
        CMatrix m = Rot; // Matrix from Quat
        return m;
    }

    CMatrix GetModelMatrix(CVector3d delta) {
        CMatrix m;
        m.ModelMatrix(Rot, delta);
        return m;
    }
    CMatrix GetModelMatrix(C3DObject *Camera) {
        return GetModelMatrix(Pos - Camera->Pos);
    }

    CMatrix GetScaledModelMatrix(CVector3d delta, double scale) {
        CMatrix m = GetModelMatrix(delta);
        double distance = delta.Magnitude();
        double factor = 1.0f;
        if (distance > ZFAR_HALF) {
            delta /= distance; // normalize delta
            factor = ZFAR_MAX;
            if (distance < ZFAR_ERR) {
                factor = ZFAR_HALF + ZFAR_HALF * (1.0f - exp((-2.5 * distance) / ZFAR_ERR));
            }
            delta *= factor;
            m.f41 = (float)delta.x;
            m.f42 = (float)delta.y;
            m.f43 = (float)delta.z;
            factor /= distance;
        }
        scale *= factor;
        m.Scale((float)scale, (float)scale, (float)scale);
        return m;
    }
    CMatrix GetScaledModelMatrix(C3DObject *Camera, double scale) { 
        return GetScaledModelMatrix(Pos - Camera->Pos, scale);
    }
};
