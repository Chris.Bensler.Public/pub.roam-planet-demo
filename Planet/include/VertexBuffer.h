#pragma once
#include "Global.h"
#include "Vertex.h"

#define MAX_VERTS 0x10000

class CVertexBuffer {
private:
    CVertex buffer[MAX_VERTS]; // vertex buffer
    uint16 stack[MAX_VERTS]; // FILO stack stores invalidated indices to be recycled
    uint32 count; // tracks total vertices. MAX_VERTS-count = available
    uint32 last; // tracks last\max used indice. last-count = dead\recycled

public:
    CVertexBuffer() { this->Reset(); }

    CVertex* operator [](uint16 index) { return &(this->buffer[index]); }

    void Reset() {
        this->count = this->last = 0;
        for (int i=0; i < MAX_VERTS; i++) this->stack[i] = i;
    }

    uint16 Insert(float32 x, float32 y, float32 z) {
        uint16 index;
        if (this->count >= MAX_VERTS) return -1;
        index = this->stack[this->count++];
        if (this->count > this->last) this->last++;
        this->buffer[index].pos.Set(x,y,z);
        this->buffer[index].normal.Set(x,y,z);
        this->buffer[index].normal.Normalize();
        return index;
    }
    uint16 Insert(float32 *lp) { this->Insert(lp[0],lp[1],lp[2]); }
    uint16 Insert(CVector3 v) { return this->Insert(v.x, v.y, v.z); }
    void Remove(uint16 index) { this->stack[--this->count] = index; }

    CVertex* GetBuffer() { return buffer; }

    uint32 Length() { return this->count; }
    uint32 GetTotal() { return this->last; }
    uint32 GetUnused() { return this->last - this->count; }
    uint16 GetStackIndex(uint16 index) { return this->stack[index]; }
};