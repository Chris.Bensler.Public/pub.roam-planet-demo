#include "ROAM/ROAMSphere.h"

void ROAM::CSphere::Init(float32 scale) {
    this->Scale = scale;
    this->Reset();
    // start with a cube normalized to a sphere with a RADIUS of 1

    float Verts[8][3] = {
        {-1,-1,-1} // ltn
       ,{-1, 1,-1} // lbn
       ,{ 1,-1,-1} // rtn
       ,{ 1, 1,-1} // rbn
       ,{-1,-1, 1} // ltf
       ,{-1, 1, 1} // lbf
       ,{ 1,-1, 1} // rtf
       ,{ 1, 1, 1} // rbf
    };

    // create vertices
    uint16 vIndex[8] = {0};
    CVector3 *vert;
    float64 height;
    for (int v=0; v < 8; v++) {
        vert = new CVector3(Verts[v]);
        vert->Normalize();
        height = Fractal::GetHeight(vert,this->Scale,MAX_ALTITUDE);
        *vert *= (float32) (this->Scale + ((height > 0) ? height : 0));
        vIndex[v] = this->AddVertex(*vert);
        this->VertList[vIndex[v]]->rgb = this->GetColor((float32)height/MAX_ALTITUDE);
    }

    uint16 Face[12][3] = {
        {0, 1, 2} , {3, 2, 1} // front   ltn,lbn,rtn , rbn,rtn,lbn
       ,{6, 7, 4} , {5, 4, 7} // back    rtf,rbf,ltf , lbf,ltf,rbf
       ,{4, 0, 6} , {2, 6, 0} // top     ltf,ltn,rtf , rtn,rtf,ltn
       ,{1, 5, 3} , {7, 3, 5} // bottom  lbn,lbf,rbn , rbf,rbn,lbf
       ,{4, 5, 0} , {1, 0, 5} // left    ltf,lbf,ltn , lbn,ltn,lbf
       ,{2, 3, 6} , {7, 6, 3} // right   rtn,rbn,rtf , rbf,rtf,rbn
    };

    // create root triangles
    CTriNode* Tri[12];
    for (int f=0; f < 12; f++) {
        Tri[f] = new CTriNode(NULL, vIndex[Face[f][0]], vIndex[Face[f][1]], vIndex[Face[f][2]] );
        this->TriList.tail.InsertPrev(Tri[f]);
    }

    // initialize edge pointers
    uint32 Edge[12][3] = {
        {1,5,9}, { 0,6,10}, { 3,4,11}
       ,{2,7,8}, { 5,2, 8}, { 4,0,10}
       ,{7,1,9}, { 6,3,11}, { 9,4, 3}
       ,{8,6,0}, {11,5, 1}, {10,7, 2}
    };
    for (uint32 f=0; f < 12; f++) {
        for (uint32 e=0; e < 3; e++) Tri[f]->edge[e] = Tri[Edge[f][e]];
    }

    for (int t=0; t < 12; t+=2) {
        Tri[t]->UpdatePriority(this);
        Tri[t+1]->CopyPriority(this);
    }
}

uint16 ROAM::CSphere::Tesselate(uint16 iterations) {
    uint16 i = iterations;
    while (i--) if (!this->SplitAll()) break;
    return iterations-i;
}

void ROAM::CSphere::Simplify(uint16 iterations) {
    while (iterations--) this->MergeAll();
}

bool ROAM::CSphere::SplitAll() {
  CTriNode *tri;
  uint16 lod;
    tri = this->TriList.GetFirst();
    lod = tri->lod;
    for (tri; !this->TriList.IsTail(tri); tri = tri->next) {
        if (tri->lod == lod) this->Split(tri);
    }
    return true;
}

void ROAM::CSphere::MergeAll() {
  uint16 lod = 0;
    if (this->TriList.Count() <= 12) return;
    // get highest lod
    for (CQuadNode *quad = this->QuadList.GetFirst(); !this->QuadList.IsTail(quad); quad = quad->next) {
        if (lod < quad->parent->lod) lod = quad->parent->lod;
    }
    
    for (CQuadNode *quad = this->QuadList.GetFirst(); !this->QuadList.IsTail(quad); quad = quad->next) {
        if (quad->parent->lod == lod) {
            quad = quad->prev;
            this->Merge(quad->next);
        }
    }
}

// Split
// returns false if can't split
bool ROAM::CSphere::Split(CTriNode* tri) {
  CTriNode* root[2];
  CTriNode* child[2];
  CPriority priority;
  CQuadNode *quad;

    if (!tri->CanSplit()) { // too big, it must be split first
        if (!this->Split(tri->edge[0])) return false; // didn't split, cancel
    }
    if (this->VertList.Length() >= MAX_VERTS) return false; // no more vertices available

    root[0] = tri; // selected tri
    root[1] = tri->edge[0]; // opposite tri
    priority.CopyPriority(root[0]); // store to make new quad later

    // if either of the root triangles are part of a quad, then delete the quad
    if (root[0]->quad) delete root[0]->quad;
    if (root[1]->quad) delete root[1]->quad;

    // add midpoint to vertex list and scale to unit radius
    uint16 vMidpoint = this->AddVertex(root[0]->target);
    this->VertList[vMidpoint]->rgb = this->GetColor((float32)(root[0]->height) / MAX_ALTITUDE);

    // create 2 new child triangles
    child[0] = new CTriNode(root[0],vMidpoint,root[1]->vert[1],root[0]->vert[0]);
    child[1] = new CTriNode(root[1],vMidpoint,root[0]->vert[1],root[1]->vert[0]);

    // update vertices for the two root triangles (create children first!)
    root[0]->SetVerts(vMidpoint, root[0]->vert[0], root[0]->vert[1]);
    root[1]->SetVerts(vMidpoint, root[1]->vert[0], root[1]->vert[1]);

    // update the adjacent edge pointers (before updating root edges!)
    root[0]->edge[1]->ReplaceEdge(root[0],child[0]);
    root[1]->edge[1]->ReplaceEdge(root[1],child[1]);

    // setup child edge pointers
    child[0]->SetEdges(root[0]->edge[1],  root[0],  root[1]);
    child[1]->SetEdges(root[1]->edge[1],  root[1],  root[0]);

    // update root edge pointers. (set child edges first!)
    root[0]->SetEdges(root[0]->edge[2], child[1], child[0]);
    root[1]->SetEdges(root[1]->edge[2], child[0], child[1]);

    // increment lod for root triangles
    root[0]->lod++;   root[1]->lod++;

    // update priority info. (update all edges first!)
    root[0]->CopyPriority(this);    child[0]->CopyPriority(this);
    root[1]->CopyPriority(this);    child[1]->CopyPriority(this);
    root[0]->SetWait(1); root[1]->SetWait(1); child[0]->SetWait(1); child[1]->SetWait(1);

    // link children
    root[0]->InsertNext(child[0]);
    root[1]->InsertNext(child[1]);

    // update normals
    root[0]->UpdateNormals(this);
    root[1]->UpdateNormals(this);

    // add a new quad
    quad = new CQuadNode(root[0]);
    quad->CopyPriority(&priority);
    this->QuadList.InsertLast(quad);

    return true;
}

// Merge
void ROAM::CSphere::Merge(CQuadNode *quad) {
  CTriNode *root[2], *child[2];
  CPriority priority;
  CTriNode tri;

    priority.CopyPriority(quad); // store for later

    root[0] = quad->parent;
    child[0] = root[0]->edge[2];
    root[1] = child[0]->edge[2];
    child[1] = root[1]->edge[2];

    this->DelVertex(root[0]->vert[0]); // remove the shared midpoint vertex
    delete quad; // delete before we delete any triangles

    for (int i=0; i < 2; i++) {
        root[i]->lod--;
        root[i]->SetVerts(root[i]->vert[1], root[i]->vert[2], child[i]->vert[1]);
        root[i]->SetEdges(root[1-i],child[i]->edge[0],root[i]->edge[0]);
        root[i]->edge[1]->ReplaceEdge(child[i],root[i]);
        ((CPriority*)root[i])->CopyPriority(&priority); // update priority info
        root[i]->SetWait(0);

        // do we need to create a new quad?
        if (root[i]->CanMerge()) {
            CQuadNode *q = new CQuadNode(root[i]);
            if (root[i] != root[i]->edge[2]->parent) q->parent = root[i]->edge[2]; // if root[i] is not a parent, then adjust
            this->QuadList.InsertLast(q);
            // set priority info for the quad.
            tri.vert[0] = root[i]->vert[0];
            tri.vert[1] = root[i]->edge[2]->vert[1]; // calculate based on the quad size, not the half size
            tri.vert[2] = root[i]->vert[2];
            tri.UpdatePriority(this);
            q->CopyPriority(&tri);
        }
        delete child[i];
    }
}

void ROAM::CSphere::Update(CVector position, CVector heading, float64 horizon, float64 max_error) {
  const float64 min_error = 1e-6f;
  float64 twice_error = max_error*2.0f;
  float64 half_error = max_error/2.0f;
  float64 error;
  
    // merge quads
    for (CQuadNode *quad=this->QuadList.GetFirst(); !this->QuadList.IsTail(quad); quad = quad->next) {
        if (!quad->GetWait()) {
            error = quad->GetError(position, heading, horizon);
            if (error < max_error) {
                quad = quad->prev;
                this->Merge(quad->next);
            } else {
                quad->SetWait((error > twice_error) ? 2 : 1);
            }
        }
    }

    // split tris
    CTriNode *tri = this->TriList.GetFirst();
    CTriNode *last = this->TriList.GetLast();
    for (; tri->prev != last; tri = tri->next) {
        if (!tri->GetWait()) {
            error = tri->GetError(position, heading, horizon);
            if (error >= max_error) {
                this->Split(tri);
            } else {
                uint32 priority = (uint32) (1.0f/max_error/error * 1) + 1;
                tri->SetWait(priority);
            }
        }
    }

    // build index list
    this->TCount = 0;
    for (CTriNode *tri = this->TriList.GetFirst(); tri != &(this->TriList.tail); tri = tri->next) {
        this->Index[this->TCount++] = tri->vert[0];
        this->Index[this->TCount++] = tri->vert[1];
        this->Index[this->TCount++] = tri->vert[2];
    }
}
